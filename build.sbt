name := "bsp-sbt"
organization := "com.gitlab.fduraffourg"
sbtPlugin := true
scalaVersion := "2.12.8"
scalafmtOnCompile := true
version := "0.0-SNAPSHOT"
libraryDependencies ++= Seq(
    "ch.epfl.scala" %% "bsp4s" % "2.0.0-M4",
    "com.eed3si9n" %%  "sjson-new-scalajson" % "0.8.3"
)


package sbt.bsp

import sbt._
import sbt.Keys.onLoad
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import sbt.internal.langserver.codec.JsonProtocol._
import sbt.internal.server._
import sbt.internal.protocol._
import ch.epfl.scala.bsp._
import sjsonnew._
import sjsonnew.support.scalajson.unsafe.Converter
import com.bestmile.sbt.Formaters._
import com.bestmile.sbt.Handlers._
import scala.util.Success

object Bsp extends AutoPlugin {
  override def trigger  = allRequirements
  override def requires = empty

  object autoImport {}

  override val globalSettings = Seq(
    onLoad in Global := { state =>
      val host = Keys.serverHost.value
      val port = Keys.serverPort.value
      writeSbtConfig(host, port)
      state
    },
    Keys.serverConnectionType := ConnectionType.Tcp,
    Keys.serverHandlers += {
      val loadedBuild = Keys.loadedBuild.value
      bspServerHandlers(loadedBuild.allProjectRefs)
    },
    Keys.commands ++= Seq(replyToBuildTargetScalacOptions, replyToBuildTargetSources)
  )

  def replyToBuildTargetScalacOptions = Command.args("replyToBuildTargetScalacOptions", "<channelName> <requestID>") {
    (state, args) =>
      val channelName +: requestID +: _ = args
      val extracted                     = Project.extract(state)
      val structure                     = extracted.structure
      val settings                      = structure.data
      val config                        = EvaluateTask.extractedTaskConfig(extracted, structure, state)

      val items = Keys.loadedBuild.get(settings) match {
        case Some(loadedBuild) =>
          loadedBuild.allProjectRefs.map {
            case (projectRef, resolvedProject) =>
              val scalacOptions =
                EvaluateTask(structure, Keys.scalacOptions.in(Compile), state, projectRef, config).map {
                  case (_, result) => result.toEither
                }.flatMap {
                  case Right(value) => Some(value)
                  case Left(value)  => None
                }.getOrElse(Nil).toList

              val classpath = EvaluateTask(structure, Keys.fullClasspath.in(Compile), state, projectRef, config).map {
                case (_, result) => result.toEither
              }.flatMap {
                case Right(value) => Some(value)
                case Left(value)  => None
              }.getOrElse(Nil).toList.map(attributed => Uri(attributed.data.toURI.toString))

              val classdirectory =
                Keys.classDirectory
                  .in(projectRef, Compile)
                  .get(settings)
                  .map(file => Uri(file.toURI.toString))
                  .getOrElse(Uri(""))

              ScalacOptionsItem(
                target = BuildTargetIdentifier(Uri(projectRef.build.toString)),
                options = scalacOptions,
                classpath = classpath,
                classDirectory = classdirectory,
              )
          }
        case None =>
          Nil
      }

      val reply    = ScalacOptionsResult(items.toList)
      val channels = StandardMain.exchange.channels
      channels.foreach {
        case c: NetworkChannel if c.name == channelName =>
          c.jsonRpcRespond(reply, Some(requestID))
        case other => ()
      }

      state
  }

  def replyToBuildTargetSources = Command.args("replyToBuildTargetSources", "<channelName> <requestID>") {
    (state, args) =>
      val channelName +: requestID +: _ = args
      val extracted                     = Project.extract(state)
      val structure                     = extracted.structure
      val settings                      = structure.data
      val config                        = EvaluateTask.extractedTaskConfig(extracted, structure, state)

      val items = Keys.loadedBuild.get(settings) match {
        case Some(loadedBuild) =>
          loadedBuild.allProjectRefs.map {
            case (projectRef, resolvedProject) =>
              val sources = EvaluateTask(structure, Keys.sources.in(Compile), state, projectRef, config).map {
                case (_, result) => result.toEither
              }.flatMap {
                case Right(files) =>
                  Some(files.map { file =>
                    SourceItem(
                      uri = Uri(file.toURI.toString),
                      kind = SourceItemKind.File,
                      generated = false,
                    )
                  })
                case Left(value) => None
              }.getOrElse(Nil).toList

              SourcesItem(
                target = BuildTargetIdentifier(Uri(projectRef.build)),
                sources = sources,
              )
          }
        case None =>
          Nil
      }

      val reply    = SourcesResult(items.toList)
      val channels = StandardMain.exchange.channels
      channels.foreach {
        case c: NetworkChannel if c.name == channelName =>
          c.jsonRpcRespond(reply, Some(requestID))
        case other => ()
      }

      state
  }

  private def writeSbtConfig(host: String, port: Int) = {
    val content = s"""{
      "name": "Sbt",
      "version": "0.0",
      "bspVersion": "2.0.0",
      "languages": ["scala"],
      "argv": ["socat", "STDIO", "TCP:$host:$port"]
    }
    """
    IO.write(file(".bsp/sbt.json"), content)
  }

  private def bspServerHandlers(
    allProjectRefs: Seq[(ProjectRef, ResolvedProject)]
  ) =
    ServerHandler({ callback =>
      ServerIntent(
        {
          case r: JsonRpcRequestMessage if r.method == "build/initialize" =>
            val response = handleBuildInitialize()
            callback.jsonRpcRespond(response, Some(r.id))

          case r: JsonRpcRequestMessage if r.method == "workspace/buildTargets" =>
            val response = handleWorkspaceBuildTargets(allProjectRefs)
            callback.jsonRpcRespond(response, Some(r.id))

          case r: JsonRpcRequestMessage if r.method == "buildTarget/scalacOptions" =>
            // val params = r.params.map(json => Converter.fromJson[ScalacOptionsParams](json))
            // val projects = params match {
            //   case Some(Success(ScalacOptionsParams(targets))) =>
            //     val builds = targets.map(_.uri.value)
            //     allProjectRefs.filter { case (projectRef, _) => builds.contains(projectRef.build.toString()) }
            //   case _ => Nil
            // }
            callback.appendExec(
              Exec(s"replyToBuildTargetScalacOptions ${callback.name} ${r.id}", Some(CommandSource(callback.name)))
            )

          case r: JsonRpcRequestMessage if r.method == "buildTarget/sources" =>
            callback.appendExec(
              Exec(s"replyToBuildTargetSources ${callback.name} ${r.id}", Some(CommandSource(callback.name)))
            )

          case r: JsonRpcRequestMessage if r.method == "build/shutdown" =>
            callback.jsonRpcRespond((), Some(r.id))

          case r: JsonRpcRequestMessage if r.method == "buildTarget/dependencySources" =>
            val params                 = r.params.map(json => Converter.fromJson[DependencySourcesParams](json))
            val buildTargetIdentifiers = params.flatMap(_.toOption).map(_.targets).getOrElse(Nil)
            val items = buildTargetIdentifiers.map { id =>
              DependencySourcesItem(id, Nil)
            }
            callback.jsonRpcRespond(DependencySourcesResult(items), Some(r.id))
        },
        PartialFunction.empty
      )

    })
}

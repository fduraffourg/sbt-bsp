package com.bestmile.sbt

import sbt._
import Keys._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import sbt.internal.langserver.codec.JsonProtocol._
import sbt.internal.server._
import sbt.internal.protocol._
import ch.epfl.scala.bsp._
import sjsonnew._
import com.bestmile.sbt.Formaters._

object Handlers {
  def handleBuildInitialize(): InitializeBuildResult =
    InitializeBuildResult(
      displayName = "sbt-bsp",
      version = "0.0",
      bspVersion = "2.0.0-M4",
      capabilities = BuildServerCapabilities(
        compileProvider = Some(CompileProvider(List("scala", "java"))),
        testProvider = None,
        runProvider = None,
        inverseSourcesProvider = None,
        dependencySourcesProvider = None,
        resourcesProvider = None,
        buildTargetChangedProvider = None
      ),
      data = None,
    )

  def handleWorkspaceBuildTargets(allProjectRefs: Seq[(ProjectRef, ResolvedProject)]): WorkspaceBuildTargetsResult = {
    val targetCapabilities = BuildTargetCapabilities(
      canCompile = true,
      canTest = true,
      canRun = true
    )

    val targets = allProjectRefs.map {
      case (projectRef, project) =>
        val dependencies = project.dependencies.map { dependency =>
          BuildTargetIdentifier(Uri(dependency.project.build.toString))
        }.toList
        BuildTarget(
          id = BuildTargetIdentifier(Uri(projectRef.build.toString)),
          displayName = Some(projectRef.project),
          baseDirectory = Some(Uri(project.base.toURI().toString())),
          tags = Nil,
          capabilities = targetCapabilities,
          languageIds = List("scala"),
          dependencies = dependencies,
          dataKind = Some("scala"),
          data = None,
        )
    }.toList

    WorkspaceBuildTargetsResult(targets)
  }
}

package com.bestmile.sbt

import sbt._
import Keys._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import sbt.internal.langserver.codec.JsonProtocol._
import sbt.internal.server._
import sbt.internal.protocol._
import ch.epfl.scala.bsp._
import sjsonnew._

object Formaters {
  implicit object uriFormat extends JsonFormat[Uri] {
    def write[J](i: Uri, builder: Builder[J]): Unit =
      builder.writeString(i.value)

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = jsOpt match {
      case Some(js) => Uri(unbuilder.readString(js))
      case None     => deserializationError("Expected String, got None")
    }
  }

  implicit object compileProviderFormat extends JsonFormat[CompileProvider] {
    def write[J](i: CompileProvider, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("languageIds", i.languageIds)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object buildServerCapabilitiesFormat extends JsonFormat[BuildServerCapabilities] {
    def write[J](i: BuildServerCapabilities, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("compileProvider", i.compileProvider)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) =
      jsOpt match {
        case Some(js) => BuildServerCapabilities(None, None, None, None, None, None, None)
        case None     => deserializationError("Expected jsOvbject but found None")
      }
  }

  implicit object initializeBuildResultFormat extends JsonFormat[InitializeBuildResult] {
    def write[J](i: InitializeBuildResult, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("displayName", i.displayName)
      builder.addField("version", i.version)
      builder.addField("bspVersion", i.bspVersion)
      builder.addField("capabilities", i.capabilities)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) =
      jsOpt match {
        case Some(js) =>
          val caps = BuildServerCapabilities(None, None, None, None, None, None, None)
          InitializeBuildResult("", "", "", caps, None)
        case None =>
          deserializationError("Expected jsOvbject but found None")
      }
  }

  implicit object buildTargetCapabilitiesFormat extends JsonFormat[BuildTargetCapabilities] {
    def write[J](i: BuildTargetCapabilities, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("canCompile", i.canCompile)
      builder.addField("canTest", i.canTest)
      builder.addField("canRun", i.canRun)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object buildTargetIdentifierFormat extends JsonFormat[BuildTargetIdentifier] {
    def write[J](i: BuildTargetIdentifier, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("uri", i.uri)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) =
      jsOpt match {
        case Some(js) =>
          unbuilder.beginObject(js)
          val uri = unbuilder.readField[Uri]("uri")
          unbuilder.endObject()
          BuildTargetIdentifier(uri)
        case None =>
          deserializationError("Expected JsObject but found None")
      }
  }

  implicit object buildTargetFormat extends JsonFormat[BuildTarget] {
    def write[J](i: BuildTarget, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("id", i.id)
      builder.addField("displayName", i.displayName)
      builder.addField("baseDirectory", i.baseDirectory)
      builder.addField("tags", i.tags)
      builder.addField("capabilities", i.capabilities)
      builder.addField("languageIds", i.languageIds)
      builder.addField("dependencies", i.dependencies)
      builder.addField("dataKind", i.dataKind)
      // builder.addField("data", i.data)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object workspaceBuildTargetsResultFormat extends JsonFormat[WorkspaceBuildTargetsResult] {
    def write[J](i: WorkspaceBuildTargetsResult, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("targets", i.targets)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) =
      jsOpt match {
        case Some(js) =>
          WorkspaceBuildTargetsResult(Nil)
        case None =>
          deserializationError("Expected jsOvbject but found None")
      }
  }

  implicit object scalacOptionsItemFormat extends JsonFormat[ScalacOptionsItem] {
    def write[J](i: ScalacOptionsItem, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("target", i.target)
      builder.addField("options", i.options)
      builder.addField("classpath", i.classpath)
      builder.addField("classDirectory", i.classDirectory)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object scalacOptionsResultFormat extends JsonFormat[ScalacOptionsResult] {
    def write[J](i: ScalacOptionsResult, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("items", i.items)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object sourceItemKindFormat extends JsonFormat[SourceItemKind] {
    def write[J](i: SourceItemKind, builder: Builder[J]): Unit =
      i match {
        case SourceItemKind.File      => builder.writeInt(1)
        case SourceItemKind.Directory => builder.writeInt(2)
      }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object sourceItemFormat extends JsonFormat[SourceItem] {
    def write[J](i: SourceItem, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("uri", i.uri)
      builder.addField("kind", i.kind)
      builder.addField("generated", i.generated)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object sourcesItemFormat extends JsonFormat[SourcesItem] {
    def write[J](i: SourcesItem, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("target", i.target)
      builder.addField("sources", i.sources)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object dependencySourcesItemFormat extends JsonFormat[DependencySourcesItem] {
    def write[J](i: DependencySourcesItem, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("target", i.target)
      builder.addField("sources", i.sources)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object sourcesResultFormat extends JsonFormat[SourcesResult] {
    def write[J](i: SourcesResult, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("items", i.items)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object dependencySourcesResultFormat extends JsonFormat[DependencySourcesResult] {
    def write[J](i: DependencySourcesResult, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("items", i.items)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = deserializationError("No need for deserialization yet")
  }

  implicit object scalacOptionsParamsFormat extends JsonFormat[ScalacOptionsParams] {
    def write[J](i: ScalacOptionsParams, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("targets", i.targets)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = jsOpt match {
      case Some(js) =>
        unbuilder.beginObject(js)
        val targets = unbuilder.readField[List[BuildTargetIdentifier]]("targets")
        unbuilder.endObject()
        ScalacOptionsParams(targets)
      case None =>
        deserializationError("Expected JsObject but found None")
    }
  }

  implicit object dependencySourcesParamsFormat extends JsonFormat[DependencySourcesParams] {
    def write[J](i: DependencySourcesParams, builder: Builder[J]): Unit = {
      builder.beginObject()
      builder.addField("targets", i.targets)
      builder.endObject()
    }

    def read[J](jsOpt: Option[J], unbuilder: Unbuilder[J]) = jsOpt match {
      case Some(js) =>
        unbuilder.beginObject(js)
        val targets = unbuilder.readField[List[BuildTargetIdentifier]]("targets")
        unbuilder.endObject()
        DependencySourcesParams(targets)
      case None =>
        deserializationError("Expected JsObject but found None")
    }
  }
}
